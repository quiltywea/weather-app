# Troubleshooting

If you encounter problems, please go through the following steps and try again:

1. Restart the app

    Close the app, then restart it as usual.

2. Update the app

    Close the app, then open OpenStore app and check for updates for weather app.

3. Reboot the device

    Press and hold the power button to raise the system dialog and select 'reboot' there. Alternatively use the same option from the system indicator.

4. Clear the cache

    Clear the cache with the app [UT Tweak Tool](https://open-store.io/app/ut-tweak-tool.sverzegnassi) or delete the content of the *.cache/com.ubuntu.weather* folder.

5. Re-add locations

    Delete your locations in the locations list. Then add them again. Or manually delete the database in `~/local/share/com.ubuntu.weather`.

6. Delete the apps settings

    Delete the apps settings either using the UT Tweak Tool or manually by deleting the config file in `~/.config/com.ubuntu.weather`.

7. Check the log

    If the instructions above did not solve the problem, please open an issue and provide the app log. One way to obtain the log is with [Logviewer app](https://open-store.io/app/logviewer.ruditimmer). This app allows uploading the log to a pastebin. Provide the link to the pasted log in the error report. Another option is to manually copy the log file. It can be found under *./cache/upstart/application-click-com.ubuntu.weather_weather_VERSION.log*.

    If no logfile for weather app is present, please temporary enable developer mode (system settings -> info -> developer mode). This will make the log persist.

8. Open an issue on gitlab

    Please go to [https://gitlab.com/ubports/apps/weather-app/-/issues](https://gitlab.com/ubports/apps/weather-app/-/issues) and file an error report there. Please provide the following information:
    - the version of the app
    - the name of your device
    - your release channel
    - a description of what you did and which problems you encountered
