v5.11.2
- fix: incorrect weather icons when using inch as downfall unit (thanks @myii)
- fix: #92 when locations can't be loaded by adding some basic error parsing and providing an improved error message
- fix: UI error of weather icon overlapping with temperature in location list
- updated: translations, many thanks to all translators!

v5.11.1
- added: station/city id and coordinates to station list and search results
- updated: translations, many thanks to all translators!

v5.11.0
- improved: added some more instructions to troubleshooting
- improved: added troubleshooting to README in gitlab
- improved: rework settings page to use OptionSelector to match other core apps
- fixed: troubleshooting section not visible, splitted about content into about and help pages
- fixed: wrong unit symbol shown for rain downfall l/m²
- updated: build configuration now using clickable version 7 (thanks @jonnius)
- updated: framework raised to 16.04.5
- updated: raised QtQuick import from 5.9 to 5.12
- updated: translations, many thanks to all translators!

v5.10.1
- improved: in location search show admin3 name, if name+admin1+admin2 yield multiple identical results
- updated: translations, many thanks to all translators!

v5.10.0
- added: troubleshooting section in about page
- updated: translations, many thanks to all translators!

v5.9.0
- fixed: #75 GPS based location not working
- fixed: #86 locale city names and description not working for some languages
- fixed: wind speed unit m/s not using the locales decimal separator
- fixed: none or wrong default values used for some settings
- fixed: not working check for am/pm time formats to allow more width for hourly forecast
- added: chance of precipitation to hourly and daily forecast (where available), thanks to openweathermap.org for adding this to the API
- added: l/m² as unit for rain
- improved: internally rework units to allow more flexible translation e.g. regarding space between number and unit
- improved: redesign about page now with 'general', 'usage' and 'changelog' subpages
- removed: https url dispatcher for OWM since this conflicts with other apps
- updated: translations, many thanks to all translators!

v5.8.2
- added: link to full changelog in about page
- updated: translations, many thanks to all translators!

v5.8.1
- updated: translations, many thanks to all translators!

v5.8.0
- fixed: #83 automatic data refresh not working
- added: new icons for snow and sleet based on downfall volume to hourly and daily forecasts
- updated: translations, many thanks to all translators!

v5.7.0
- fixed: #78 inconsistent am/pm notation for hourly and suntimes
- fixed: hourly forecast missing am/pm notations for some locales
- improved: date label in daily forecast can take up more space to allow more localized dates to be fully displayed
- updated: Momentjs with the included IANA timezone database (now v2020a)
- updated: translations, many thanks to all translators!

v5.6.0
- fixed: #80 now correct wind speeds are displayed with current values (before daily values)
- fixed: #81 small redesign for visual separation of current data and day data
- fixed: #82 implement new forecast rain icons for hourly and daily forecast (thanks @cibersheep for the icons)
- added: current cloud coverage value
- improved: with m/s as unit for wind speed, now 1 decimal is given
- improved: vertical alignment of daily forecast information
- updated: translations, many thanks to all translators!

v5.5.3
- fixed: some error messages with upcoming Qt5.12 (thanks @dobey)
- updated: translations, many thanks to all translators!

v5.5.2
- fixed: some translations not using abbreviated units
- improved: smaller UI changes to current weather for better handling of long condition texts
- updated: translations, many thanks to all translators!

v5.5.1
- fixed: feels like string not being translated

v5.5.0
- fixed: #76 replace kph with m/s as default wind speed unit unit
- added: "feels like" temperature to current info (if available at your location)
- added: rain volume to hourly data and day extended info section
- added: setting for rain volume unit
- added: m/s as unit for wind speed (official SI unit)
- improved: restructure internal unit handling and default unit values

v5.4.0
- fixed: #73 locations and location search now uses localized city names (if available at geonames.org). Existing locations need to be removed and added again to use the localized name.
- fixed: #74 predefined list of cities now contains cities with more than 5 million inhabitants (according to geonames.org data)
- fixed: #46 animation when changing locations from indicator or location list
- improved: redesign of location search to always show search bar
- updated: translations, many thanks to all translators!

v5.3.0
- fixed: #64 mismatch of icon and text for current weather condition
- fixed: #72 wrong sunrise and sunset times in places with daylightsaving
- added: about page including credits
- improved: remapped icons for cloud and rain to show more separate conditions
- updated: translations, many thanks to all translators!

v5.2.2
- updated: translations, many thanks to all translators!

v5.2.1
- removed: color select option for highlight today, provide grey or none for highlighting

v5.2.0
- fixed: #59 option to set background color for todays weather info
- added: show pressure in extended information section
- removed: setting to toggle pressure information
- removed: dividers for a consistent appwide appearance

v5.1.5
- updated: translations, many thanks to all translators!

v5.1.3
- updated: translations, many thanks to all translators!

v5.1.2
- improved: use themed colors for blue and red
- improved: some small UI changes to rain radar icon and header
- updated: translations, many thanks to all translators!

v.5.1.1
- fixed: settings page not showing correct version number
- improved: new icons for rain radar map to make usage more intuitive

v.5.1.0
- fixed: button margins in networkError page
- added: weather radar provided by rainviewer.com (many thanks!)

v5.0.6
- fixed: #66 sunrise and sunset times off by one hour in areas with daylight saving
- updated: translations, many thanks to all translators!

v5.0.5
- fixed: temperature color setting shows the current selected value as subtitle
- updated: translations, many thanks to all translators!

v5.0.4
- fixed: #63 theme not applied to non colored temperatures
- updated: translations, many thanks to all translators!

v5.0.3
- updated: translations, many thanks to all translators!

v5.0.2
- improved: exchange moon emojis with svg-graphics (many thanks to @cibersheep for the artwork!)

v5.0.1
- fixed: #61 adjust spacing between daily and hourly forecast
- added: blue/yellow color theme for high/low temperatures
- improved: reduze size of dots to match app scopes
- updated: translations, many thanks to all translators!

v5.0.0
- fixed: #44 finish weather app redesign
  * fixed: #54 make colors more consistent
  * added: page indicator if more than one location is set
  * added: up/down icon to day details
  * improved: hourly forecast always visible
- fixed: #55 spacing/height of moonphase text, moon emoji infront of description
- fixed: #56 bottom edge not working on theme change
- fixed: bad alignment on daily forecast and locations
- fixed: #57 add offline mode and network available indicator
- added: pressure data values
- added: country name to location in location list
- added: allow refresh intervals of up to 4 hours
- added: info page for "hidden" functions in locations page
- improved: settings page only flickable if needed
- updated: translations, many thanks to all translators!

v4.5.2
- updated: translations, many thanks to all translators!

v4.5.1
- fixed: small visibility issue in settings page

v4.5.0
- fixed: #51 add option to use system theme
- fixed: #53 sunset/sunrise times now shown in their local time (thanks Lorenzo)
- added: icon for expanding/collapsing daily details page
- improved: add wordwrap for long moonphase strings due to translation

v.4.4.2
- updated: translations, many thanks to all translators!

v4.4.1
- improved: small internal tweak

v.4.4.0
- added: moonphase emojis for visualization
- improved: small improvements to moonphase calculations
- improved: omit 0% humidity values (forecast only available for 3 days)
- updated: suncalc.js from its repo

v4.3.1
- fixed: missing api key

v4.3.0
- fixed: #48 corrected sunrise and sunset times calculation
- added: moonphase information

v4.2.0
- added: weekday and calendar date show in week view

v4.1.0
- added: new dark mode toggle
- improved: redesigned setting page

v4.0.0
- added: dark theme support
- improved: redesigned icons
- improved: the app can now be rotated to a landscape view
- improved: splash screen

v3.6.2 & v3.6.3 & v3.6.4 & v3.6.5 & v3.6.6
- updated: translations, many thanks to all translators!

v3.6.1
- removed: Temporarily removed "The Weather Channel" data provider. UBports only has an api key for OpenWeatherMap at this time. We apologize for the inconvenience. We hope to add the option to input your own api key for different services in the future.

v3.6.0
- updated: translations, many thanks to all translators!

before v3.6.0
- not documented, please check commit history in the repo
