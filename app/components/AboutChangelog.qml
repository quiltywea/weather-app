/*
 * Copyright (C) 2021 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Page {
    id: aboutChangelogPage

    Flickable {
        id: aboutFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded
        boundsBehavior: Flickable.StopAtBounds

        anchors {
            topMargin: units.gu(2)
            fill: parent
        }

        contentHeight: changelogColumn.childrenRect.height

        Column {
            id: changelogColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Repeater {
                id: changelogRepeater

                model: [
                    // empty template for changelog
                    // {
                    //     version: "v",
                    //     changes: [
                    //         {value: i18n.ctr("changelog","")},
                    //         {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                    //     ]
                    // },
                    {
                        version: "v5.11.2",
                        changes: [
                            {value: i18n.ctr("changelog","fix: incorrect weather icons when using inch as downfall unit (thanks @myii)")},
                            {value: i18n.ctr("changelog","fix: #92 when locations can't be loaded by adding some basic error parsing and providing an improved error message")},
                            {value: i18n.ctr("changelog","fix: UI error of weather icon overlapping with temperature in location list")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.11.1",
                        changes: [
                            {value: i18n.ctr("changelog","added: station/city id and coordinates to station list and search results")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.11.0",
                        changes: [
                            {value: i18n.ctr("changelog","improved: added some more instructions to troubleshooting")},
                            {value: i18n.ctr("changelog","improved: added troubleshooting to README in gitlab")},
                            {value: i18n.ctr("changelog","improved: rework some strings to allow better translation")},
                            {value: i18n.ctr("changelog","improved: rework settings page to use OptionSelector to match other core apps")},
                            {value: i18n.ctr("changelog","fixed: troubleshooting section not visible, splitted about content into about and help pages")},
                            {value: i18n.ctr("changelog","fixed: wrong unit symbol shown for rain downfall l/m²")},
                            {value: i18n.ctr("changelog","updated: build configuration now using clickable version 7 (thanks @jonnius)")},
                            {value: i18n.ctr("changelog","updated: framework raised to 16.04.5")},
                            {value: i18n.ctr("changelog","updated: raised QtQuick import from 5.9 to 5.12")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },{
                        version: "v5.10.1",
                        changes: [
                            {value: i18n.ctr("changelog","improved: in location search show admin3 name, if name+admin1+admin2 yield multiple identical results")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.10.0",
                        changes: [
                            {value: i18n.ctr("changelog","added: troubleshooting section in about page")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.9.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #75 GPS based location not working")},
                            {value: i18n.ctr("changelog","fixed: #86 locale city names and description not working for some languages")},
                            {value: i18n.ctr("changelog","fixed: wind speed unit m/s not using the locales decimal separator")},
                            {value: i18n.ctr("changelog","fixed: none or wrong default values used for some settings")},
                            {value: i18n.ctr("changelog","fixed: not working check for am/pm time formats to allow more width for hourly forecast")},
                            {value: i18n.ctr("changelog","added: chance of precipitation to hourly and daily forecast (where available), thanks to openweathermap.org for adding this to the API")},
                            {value: i18n.ctr("changelog","added: l/m² as unit for rain")},
                            {value: i18n.ctr("changelog","improved: internally rework units to allow more flexible translation e.g. regarding space between number and unit")},
                            {value: i18n.ctr("changelog","improved: redesign about page now with 'general', 'usage' and 'changelog' subpages")},
                            {value: i18n.ctr("changelog","removed: https url dispatcher for OWM since this conflicts with other apps")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.8.2",
                        changes: [
                            {value: i18n.ctr("changelog","added: link to full changelog in about page")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.8.1",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.8.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #83 automatic data refresh not working")},
                            {value: i18n.ctr("changelog","added: new icons for snow and sleet based on downfall volume to hourly and daily forecasts")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.7.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #78 inconsistent am/pm notation for hourly and suntimes")},
                            {value: i18n.ctr("changelog","fixed: hourly forecast missing am/pm notations for some locales")},
                            {value: i18n.ctr("changelog","improved: date label in daily forecast can take up more space to allow more localized dates to be fully displayed")},
                            {value: i18n.ctr("changelog","updated: Momentjs with the included IANA timezone database (now v2020a)")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.6.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #80 now correct wind speeds are displayed with current values (before daily values)")},
                            {value: i18n.ctr("changelog","fixed: #81 small redesign for visual separation of current data and day data")},
                            {value: i18n.ctr("changelog","fixed: #82 implement new forecast rain icons for hourly and daily forecast (thanks @cibersheep for the icons)")},
                            {value: i18n.ctr("changelog","added: current cloud coverage value")},
                            {value: i18n.ctr("changelog","improved: with m/s as unit for wind speed, now 1 decimal is given")},
                            {value: i18n.ctr("changelog","improved: vertical alignment of daily forecast information")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.5.3",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: some error messages with upcoming Qt5.12 (thanks @dobey)")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.5.2",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: some translations not using abbreviated units")},
                            {value: i18n.ctr("changelog","improved: smaller UI changes to current weather for better handling of long condition texts")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.5.1",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: feels like string not being translated")}
                        ]
                    },
                    {
                        version: "v5.5.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #76 replace kph with m/s as default wind speed unit unit")},
                            {value: i18n.ctr("changelog","added: \"feels like\" temperature to current info (if available at your location)")},
                            {value: i18n.ctr("changelog","added: rain volume to hourly data and day extended info section")},
                            {value: i18n.ctr("changelog","added: setting for rain volume unit")},
                            {value: i18n.ctr("changelog","added: m/s as unit for wind speed (official SI unit)")},
                            {value: i18n.ctr("changelog","improved: restructure internal unit handling and default unit values")}
                        ]
                    },
                    {
                        version: "v5.4.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #73 locations and location search now uses localized city names (if available at geonames.org). Existing locations need to be removed and added again to use the localized name.")},
                            {value: i18n.ctr("changelog","fixed: #74 predefined list of cities now contains cities with more than 5 million inhabitants (according to geonames.org data)")},
                            {value: i18n.ctr("changelog","fixed: #46 animation when changing locations from indicator or location list")},
                            {value: i18n.ctr("changelog","improved: redesign of location search to always show search bar")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.3.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #64 mismatch of icon and text for current weather condition")},
                            {value: i18n.ctr("changelog","fixed: #72 wrong sunrise and sunset times in places with daylightsaving")},
                            {value: i18n.ctr("changelog","added: about page including credits")},
                            {value: i18n.ctr("changelog","improved: remapped icons for cloud and rain to show more separate conditions")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.2.2",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.2.1",
                        changes: [
                            {value: i18n.ctr("changelog","removed: color select option for highlight today, provide grey or none for highlighting")}
                        ]
                    },
                    {
                        version: "v5.2.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #59 option to set background color for todays weather info")},
                            {value: i18n.ctr("changelog","added: show pressure in extended information section")},
                            {value: i18n.ctr("changelog","removed: setting to toggle pressure information")},
                            {value: i18n.ctr("changelog","removed: dividers for a consistent appwide appearance")}
                        ]
                    },
                    {
                        version: "v5.1.5",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.1.3",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.1.2",
                        changes: [
                            {value: i18n.ctr("changelog","improved: use themed colors for blue and red")},
                            {value: i18n.ctr("changelog","improved: some small UI changes to rain radar icon and header")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.1.1",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: settings page not showing correct version number")},
                            {value: i18n.ctr("changelog","improved: new icons for rain radar map to make usage more intuitive")}
                        ]
                    },
                    {
                        version: "v5.1.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: button margins in networkError page")},
                            {value: i18n.ctr("changelog","added: weather radar provided by rainviewer.com (many thanks!)")}
                        ]
                    },
                    {
                        version: "v5.0.6",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #66 sunrise and sunset times off by one hour in areas with daylight saving")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.0.5",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: temperature color setting shows the current selected value as subtitle")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.0.4",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #63 theme not applied to non colored temperatures")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.0.3",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.0.2",
                        changes: [
                            {value: i18n.ctr("changelog","improved: exchange moon emojis with svg-graphics (many thanks to @cibersheep for the artwork!)")}
                        ]
                    },
                    {
                        version: "v5.0.1",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #61 adjust spacing between daily and hourly forecast")},
                            {value: i18n.ctr("changelog","added: blue/yellow color theme for high/low temperatures")},
                            {value: i18n.ctr("changelog","improved: reduze size of dots to match app scopes")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v5.0.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #44 finish weather app redesign\n* fixed: #54 make colors more consistent\n* added: page indicator if more than one location is set\n* added: up/down icon to day details\n* improved: hourly forecast always visible")},
                            {value: i18n.ctr("changelog","fixed: #55 spacing/height of moonphase text, moon emoji infront of description")},
                            {value: i18n.ctr("changelog","fixed: #56 bottom edge not working on theme change")},
                            {value: i18n.ctr("changelog","fixed: bad alignment on daily forecast and locations")},
                            {value: i18n.ctr("changelog","fixed: #57 add offline mode and network available indicator")},
                            {value: i18n.ctr("changelog","added: pressure data values")},
                            {value: i18n.ctr("changelog","added: country name to location in location list")},
                            {value: i18n.ctr("changelog","added: allow refresh intervals of up to 4 hours")},
                            {value: i18n.ctr("changelog","added: info page for \"hidden\" functions in locations page")},
                            {value: i18n.ctr("changelog","improved: settings page only flickable if needed")},
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v4.5.2",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v4.5.1",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: small visibility issue in settings page")}
                        ]
                    },
                    {
                        version: "v4.5.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #51 add option to use system theme")},
                            {value: i18n.ctr("changelog","fixed: #53 sunset/sunrise times now shown in their local time (thanks Lorenzo)")},
                            {value: i18n.ctr("changelog","added: icon for expanding/collapsing daily details page")},
                            {value: i18n.ctr("changelog","improved: add wordwrap for long moonphase strings due to translation")}
                        ]
                    },
                    {
                        version: "v4.4.2",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v4.4.1",
                        changes: [
                            {value: i18n.ctr("changelog","improved: small internal tweak")}
                        ]
                    },
                    {
                        version: "v4.4.0",
                        changes: [
                            {value: i18n.ctr("changelog","added: moonphase emojis for visualization")},
                            {value: i18n.ctr("changelog","improved: small improvements to moonphase calculations")},
                            {value: i18n.ctr("changelog","improved: omit 0% humidity values (forecast only available for 3 days)")},
                            {value: i18n.ctr("changelog","updated: suncalc.js from its repo")}
                        ]
                    },
                    {
                        version: "v4.3.1",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: missing api key")}
                        ]
                    },
                    {
                        version: "v4.3.0",
                        changes: [
                            {value: i18n.ctr("changelog","fixed: #48 corrected sunrise and sunset times calculation")},
                            {value: i18n.ctr("changelog","added: moonphase information")}
                        ]
                    },
                    {
                        version: "v4.2.0",
                        changes: [
                            {value: i18n.ctr("changelog","added: weekday and calendar date show in week view")}
                        ]
                    },
                    {
                        version: "v4.1.0",
                        changes: [
                            {value: i18n.ctr("changelog","added: new dark mode toggle")},
                            {value: i18n.ctr("changelog","improved: redesigned setting page")}
                        ]
                    },
                    {
                        version: "v4.0.0",
                        changes: [
                            {value: i18n.ctr("changelog","added: dark theme support")},
                            {value: i18n.ctr("changelog","improved: redesigned icons")},
                            {value: i18n.ctr("changelog","improved: the app can now be rotated to a landscape view")},
                            {value: i18n.ctr("changelog","improved: splash screen")}
                        ]
                    },
                    {
                        version: "v3.6.2 & v3.6.3 & v3.6.4 & v3.6.5 & v3.6.6",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "v3.6.1",
                        changes: [
                            {value: i18n.ctr("changelog","removed: Temporarily removed \"The Weather Channel\" data provider. UBports only has an api key for OpenWeatherMap at this time. We apologize for the inconvenience. We hope to add the option to input your own api key for different services in the future.")}
                        ]
                    },
                    {
                        version: "v3.6.0",
                        changes: [
                            {value: i18n.ctr("changelog","updated: translations, many thanks to all translators!")}
                        ]
                    },
                    {
                        version: "before v3.6.0",
                        changes: [
                            {value: i18n.ctr("changelog","not documented, please check commit history in the repo")}
                        ]
                    }
                ]

                delegate: Column {
                    id: delegateColumn
                    height: childrenRect.height
                    width: parent.width - units.gu(4)
                    spacing: units.gu(1)
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        right: parent.right
                        rightMargin: units.gu(2)
                    }
                    Label {
                        id: versionLabel
                        text: modelData.version
                        font.bold: true
                    }
                    /*
                    A second stacked repeater to display each entry of changes property of main repeater.
                    This allows some formatting and does make displaying independent of number of entries.
                    */
                    Repeater {
                        id: changesRepeater
                        model: modelData.changes //pass changes value as model to changesRepeater
                        delegate: Grid {
                            columns: 2
                            spacing: units.gu(0.5)
                            horizontalItemAlignment: Grid.AlignLeft
                            width: parent.width
                            Label {
                                id: strokeLabel
                                text: "-"
                                horizontalAlignment: Text.AlignLeft
                            }
                            Label {
                                id: changesLabel
                                width: parent.width - strokeLabel.width - units.gu(0.5)
                                text: modelData.value //display each element in this repeaters modelData(=changes) entry
                                wrapMode: Text.WordWrap
                                horizontalAlignment: Text.AlignJustify
                            }
                        }
                    }

                    Rectangle {
                        id: delegateSpacer
                        width: parent.width
                        height: units.gu(2)
                        color: "transparent"
                    }
                }
            }
        }
    }
}
