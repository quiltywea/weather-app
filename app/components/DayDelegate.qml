/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.1

ListItem {
    id: dayDelegate
    objectName:"dayDelegate" + index
    height: collapsedHeight

    divider { visible: false }

    property int collapsedHeight: units.gu(6.5)
    property int expandedHeight: collapsedHeight + units.gu(4) + (expandedInfo.item ? expandedInfo.item.height : 0)

    property alias day: dayLabel.text
    property alias image: weatherImage.name
    property alias high: highLabel.text
    property alias low: lowLabel.text

    property alias modelData: expandedInfo.modelData

    state: "normal"
    states: [
        State {
            name: "normal"
            PropertyChanges {
                target: dayDelegate
                height: collapsedHeight
            }
            PropertyChanges {
                target: expandedInfo
                opacity: 0
            }
            PropertyChanges {
                target: updownIcon
                name: "down"
            }
        },
        State {
            name: "expanded"
            PropertyChanges {
                target: dayDelegate
                height: expandedHeight
            }
            PropertyChanges {
                target: expandedInfo
                opacity: 1
            }
            PropertyChanges {
                target: updownIcon
                name: "up"
            }
        }
    ]

    transitions: [
        Transition {
            from: "normal"
            to: "expanded"
            SequentialAnimation {
                ScriptAction {
                    script: expandedInfo.active = true
                }
                NumberAnimation {
                    easing.type: Easing.InOutQuad
                    properties: "opacity"
                }
                ScriptAction {  // run animation to ensure the listitem fits
                    script: waitEnsureVisible.restart()
                }
            }
        },
        Transition {
            from: "expanded"
            to: "normal"
            SequentialAnimation {
                NumberAnimation {
                    easing.type: Easing.InOutQuad
                    properties: "opacity"
                }
                ScriptAction {
                    script: expandedInfo.active = false
                }
            }
        }
    ]

    onClicked: {
        state = state === "normal" ? "expanded" : "normal"
        locationPages.collapseOtherDelegates(index)
    }

    Item {
        id: mainInfo

        height: collapsedHeight
        width: units.gu(45)
        anchors {
            margins: units.gu(0)
            horizontalCenter: parent.horizontalCenter
        }

        Label {
            id: dayLabel
            anchors {
                left: parent.left
                leftMargin: units.gu(3)
                top: parent.top
                topMargin: units.gu(0.5)
                verticalCenter: fixedWidthItemsRow.vertictalCenter
            }
            //the remaining space total width minus margins minus fixedWidthItemsRow
            width: settings.tempScale === "°F" ? units.gu(13.5) : units.gu(15.5)
            elide: Text.ElideRight
            font.weight: Font.Light
            font.pixelSize: units.gu(1.8)
            horizontalAlignment: Text.AlignLeft
        }

        RowLayout {
            id: fixedWidthItemsRow
            anchors {
                right: parent.right
                rightMargin: units.gu(3)
                top: parent.top
                topMargin: units.gu(0)
            }
            //width based on the size of the items
            width: settings.tempScale === "°F" ? units.gu(25.5) : units.gu(23.5)
            spacing: units.gu(1.5)
            Icon {
                id: weatherImage
                Layout.alignment: Qt.AlignHCenter
                height: units.gu(3)
                width: height
            }

            Icon {
                id: lowImage
                Layout.alignment: Qt.AlignHCenter
                height: units.gu(2)
                width: height
                source: "../graphics/extended-information_moon.svg"
                color: settings.lowColor
            }

            Label {
                id: lowLabel
                //because Fahrenheit can go above 100 for three digits more space is needed
                Layout.preferredWidth: settings.tempScale === "°F" ? units.gu(4.5) : units.gu(3.5)
                Layout.alignment: Qt.AlignHCenter
                color: settings.lowColor
                font.weight: Font.Normal
                font.pixelSize: units.gu(2.2)
                horizontalAlignment: Text.AlignRight
            }

            Icon {
                id: highImage
                Layout.alignment: Qt.AlignHCenter
                height: units.gu(2)
                width: height
                name: "weather-clear-symbolic"
                color: settings.highColor
            }

            Label {
                id: highLabel
                //because Fahrenheit can go above 100 for three digits more space is needed
                Layout.preferredWidth: settings.tempScale === "°F" ? units.gu(4.5) : units.gu(3.5)
                Layout.alignment: Qt.AlignHCenter
                color: settings.highColor
                font.weight: Font.Normal
                font.pixelSize: units.gu(2.2)
                horizontalAlignment: Text.AlignRight
            }

            Icon {
                id: updownIcon
                Layout.alignment: Qt.AlignRight
                height: units.gu(2)
                width: height
                name: "down"
            }
        }
    }

    Loader {
        id: expandedInfo
        active: false
        anchors {
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
            top: mainInfo.bottom
            topMargin: units.gu(1)
        }
        asynchronous: true
        opacity: 0
        source: "DayDelegateExtraInfo.qml"

        property var modelData
    }

    Behavior on height {
        NumberAnimation {
            id: heightAnimation
            easing.type: Easing.InOutQuad
        }
    }

    NumberAnimation {
        // animation to ensure the listitem fits by moving the contentY
        id: ensureVisibleAnimation
        easing.type: Easing.InOutQuad
        properties: "contentY"
        target: dayDelegate.parent.parent
    }

    Timer {
        id: waitEnsureVisible
        interval: 16
        repeat: false

        onTriggered: {
            // Only trigger once the loader has loaded
            // and the animations have stopped
            // otherwise restart the timer
            if (expandedInfo.active && expandedInfo.status === Loader.Ready
                    && !heightAnimation.running) {
                // stop the current animation
                ensureVisibleAnimation.running = false;

                // Get the current position
                var view = dayDelegate.parent.parent;
                var pos = view.contentY;

                // Tell the listview to make the listitem fit
                view.positionViewAtIndex(index, ListView.Contain);

                // Animate from the original position to the new position
                ensureVisibleAnimation.from = pos;
                ensureVisibleAnimation.to = view.contentY;
                ensureVisibleAnimation.running = true;
            } else {
                restart()
            }
        }
    }

    //daily forecast does only yield "rain" icon for all intensities of rain from small drizzle to heavy rain
    //use wrapper function to determine the rain icon based on the amount of rainfall
    //if "rain" icon is transmitted, skip "image" (system icon) and use a in-app svg graphic instead
    function getRainSnowIcon(rainVolume,snowVolume,weather) {
        var resulticon
        //remove unit from rain/snow volume string and parse to float
        rainVolume = parseFloat(rainVolume.split(" ")[0].replace(",","."))
        snowVolume = parseFloat(snowVolume.split(" ")[0].replace(",","."))
        if (settings.precipUnits === "in") {
            //convert from inch to mm
            rainVolume = rainVolume*25.4;
            snowVolume = snowVolume*25.4;
        }
        if (weather === "weather-showers-symbolic" || weather === "weather-snow-symbolic") {
            if (rainVolume > 0 && (!snowVolume || snowVolume < rainVolume/5)) {
                //see https://de.wikipedia.org/wiki/Regen#Regenformen for classification
                //rain volume in mm per hour
                if (rainVolume < 0.5) { //light rain
                    resulticon = "../graphics/weather-1drop-symbolic.svg";
                } else if (rainVolume >= 0.5 && rainVolume < 4) { //medium rain
                    resulticon = "../graphics/weather-2drops-symbolic.svg";
                } else if (rainVolume >= 4 && rainVolume < 10) { //rain
                    resulticon = "../graphics/weather-3drops-small-symbolic.svg";
                } else if (rainVolume >= 10 && rainVolume < 20) { //heavy rain
                    resulticon = "../graphics/weather-3drops-big-symbolic.svg";
                } else if (rainVolume >= 20) { //very heavy rain
                    resulticon = "../graphics/weather-4drops-symbolic.svg";
                }
                setIcon(resulticon)
            } else if (snowVolume > 0 && (!rainVolume || rainVolume < snowVolume/5)) {
                //categories based on rainfall volume, see above
                //snow volume in mm per hour
                if (snowVolume < 0.5) { //light snow
                    resulticon = "../graphics/weather-1flake-symbolic.svg";
                } else if (snowVolume >= 0.5 && snowVolume < 4) { //medium snow
                    resulticon = "../graphics/weather-2flakes-symbolic.svg";
                } else if (snowVolume >= 4 && snowVolume < 10) { //snow
                    resulticon = "../graphics/weather-3flakes-small-symbolic.svg";
                } else if (snowVolume >= 10 && snowVolume < 20) { //heavy snow
                    resulticon = "../graphics/weather-3flakes-big-symbolic.svg";
                } else if (snowVolume >= 20) { //very heavy snow
                    resulticon = "../graphics/weather-4flakes-symbolic.svg";
                }
                setIcon(resulticon)
            } else if (rainVolume >= snowVolume/5 && snowVolume >= rainVolume/5){
                var combined = rainVolume + snowVolume
                if (combined < 1) {
                    resulticon = "../graphics/weather-sleet1-symbolic.svg";
                } else if (combined >= 1 && combined < 15) {
                    resulticon = "../graphics/weather-sleet2-symbolic.svg";
                } else if (combined >= 15) {
                    resulticon = "../graphics/weather-sleet3-symbolic.svg";
                }
                setIcon(resulticon)
            } else {
                resulticon = "";
            }
        }
        return resulticon;
    }
    function setIcon(icon) {
        weatherImage.source = icon;
        weatherImage.height = units.gu(3);
    }

    Component.onCompleted: {
        getRainSnowIcon(modelData.rain, modelData.snow, image)
        locationPages.collapseOtherDelegates.connect(function(otherIndex) {
            if (dayDelegate && typeof index !== "undefined" && otherIndex !== index) {
                state = "normal"
            }
        });
    }
}
