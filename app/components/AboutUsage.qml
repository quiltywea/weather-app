/*
 * Copyright (C) 2021 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

Page {
    id: aboutUsagePage

    Flickable {
        id: usageFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded
        boundsBehavior: Flickable.StopAtBounds

        anchors {
            topMargin: units.gu(2)
            fill: parent
        }

        contentHeight: usageColumn.childrenRect.height

        Column {
            id: usageColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Repeater {
                id: listViewUsage

                model: [
                    {
                        title: i18n.tr("Access weather information"),
                        description: i18n.ctr("usage, access weather information","You can swipe through locations or pick one by tapping on the indicator dots. Tap on current information or a daily forecast for additional weather details. Swipe the 3 hour forecast for more weather information.")
                    },
                    {
                        title: i18n.tr("Manage locations"),
                        description: i18n.ctr("usage, manage locations","Add multiple locations via bottom edge or enable live GPS location in settings. Manage locations via bottom edge locations page by swiping or press & hold to remove and reorder.")
                    },
                    {
                        title: i18n.tr("Locations page list actions"),
                        description: i18n.ctr("usage, manage locations","To add new locations you need to have internet access!") + "\n" + "1. " + i18n.tr("single tap a location to view its weather") + "\n" + "2. " + i18n.tr("tap and hold on a location to") + "\n" + i18n.tr("a) enable sorting mode (drag the icon to reorder)") + "\n" + i18n.tr("b) multiselect locations to delete")
                    },
                    {
                        title: i18n.tr("Rainradar"),
                        description: i18n.ctr("usage, rainradar","You can view live rainradar by tapping the satellite icon in the header.")
                    },
                    {
                        title: i18n.tr("GPS live location"),
                        description: i18n.ctr("usage, GPS live location","When GPS for current location is enabled, an additional 'live' location is added to the list. This locations updates it's position on the move according to GPS location.")
                    },
                    {
                        title: i18n.tr("Updating data"),
                        description: i18n.ctr("usage, Updating data","Weather station data is updated on app start and at the interval specified in the app settings. This does only work while the app is active.")
                    }
                ]

                delegate: Column {
                    id: delegateColumn
                    height: childrenRect.height
                    width: parent.width - units.gu(4)
                    spacing: units.gu(1)
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        right: parent.right
                        rightMargin: units.gu(2)
                    }
                    Label {
                        id: titleLabel
                        text: modelData.title
                        font.bold: true
                    }
                    Label {
                        id: descriptionLabel
                        width: parent.width
                        text: modelData.description
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignJustify
                    }
                    Rectangle {
                        id: delegateSpacer
                        width: parent.width
                        height: units.gu(2)
                        color: "transparent"
                    }
                }
            }
        }
    }
}
