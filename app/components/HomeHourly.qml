/*
 * Copyright (C) 2015 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3

ListView {
    id: homeHourly

    clip: true
    height: parent ? parent.height : undefined
    width: parent ? parent.width : undefined
    model: hourlyForecastsData.length
    orientation: ListView.Horizontal

    property string currentDate: Qt.formatTime(new Date())

    onVisibleChanged: {
        if (visible) {
            ListView.model = hourlyForecastsData.length
        }
    }

    delegate: Item {
        id: delegate

        property var hourData: hourlyForecastsData[index]

        height: parent.height
        width: childrenRect.width

        Column {
            id: hourColumn

            anchors.verticalCenter: parent.verticalCenter
            height: childrenRect.height
            //some AM/PM notations take up more space than other formats, allow some extra width for those
            width: isAmPmTimeformat() ? units.gu(11.5) : units.gu(10)

            function isAmPmTimeformat() {
                var check = ["am","a.m.","AM","A.M.","pm","p.m.","PM","P.M."]
                var isAmPmFormat = false
                for (var str in check) {
                    if (getDate(hourData.date).toLocaleTimeString(Qt.locale(),Locale.ShortFormat).search(check[str]) !== -1) {
                        isAmPmFormat = true
                    }
                }
                return isAmPmFormat
            }

            Label {
                id: timestampLabel
                anchors.horizontalCenter: parent.horizontalCenter
                fontSize: "small"
                font.weight: Font.Light
                text: "%1 %2".arg(formatTimestamp(hourData.date, 'ddd')).arg(getDate(hourData.date).toLocaleTimeString(Qt.locale(),Locale.ShortFormat))
            }

            Item {
                id: iconItem
                anchors.horizontalCenter: parent.horizontalCenter
                height: units.gu(7)
                width: units.gu(7)

                Icon {
                    id: weatherIcon
                    anchors {
                        fill: parent
                        margins: units.gu(0.5)
                    }
                    name: (hourData.icon !== undefined && iconMap[hourData.icon] !== undefined) ? iconMap[hourData.icon] : ""
                }
                //daily forecast does only yield "rain" icon for all intensities of rain from small drizzle to heavy rain
                //use wrapper function to determine the rain icon based on the amount of rainfall
                //if "rain" icon is transmitted, skip "image" (system icon) and use a in-app svg graphic instead
                function getRainSnowIcon(rainVolume,snowVolume,weather) {
                    var resulticon
                    if (weather === "rain" || weather === "snow") {
                        if (rainVolume > 0 && snowVolume < rainVolume/10) {
                              //see https://de.wikipedia.org/wiki/Regen#Regenformen for classification
                              //rain volume in mm per hour
                              if (rainVolume < 0.5) { //light rain
                                  resulticon = "../graphics/weather-1drop-symbolic.svg";
                              } else if (rainVolume >= 0.5 && rainVolume < 4) { //medium rain
                                  resulticon = "../graphics/weather-2drops-symbolic.svg";
                              } else if (rainVolume >= 4 && rainVolume < 10) { //rain
                                  resulticon = "../graphics/weather-3drops-small-symbolic.svg";
                              } else if (rainVolume >= 10 && rainVolume < 20) { //heavy rain
                                  resulticon = "../graphics/weather-3drops-big-symbolic.svg";
                              } else if (rainVolume >= 20) { //very heavy rain
                                  resulticon = "../graphics/weather-4drops-symbolic.svg";
                              }
                              weatherIcon.source = resulticon;
                        } else if (snowVolume > 0 && rainVolume < snowVolume/10) {
                              //categories based on rainfall volume, see above
                              //snow volume in mm per hour
                              if (snowVolume < 0.5) { //light snow
                                  resulticon = "../graphics/weather-1flake-symbolic.svg";
                              } else if (snowVolume >= 0.5 && snowVolume < 4) { //medium snow
                                  resulticon = "../graphics/weather-2flakes-symbolic.svg";
                              } else if (snowVolume >= 4 && snowVolume < 10) { //snow
                                  resulticon = "../graphics/weather-3flakes-small-symbolic.svg"; //TODO: create icon
                              } else if (snowVolume >= 10 && snowVolume < 20) { //heavy snow
                                  resulticon = "../graphics/weather-3flakes-big-symbolic.svg"; //TODO: create icon
                              } else if (snowVolume >= 20) { //very heavy snow
                                  resulticon = "../graphics/weather-4flakes-symbolic.svg";
                              }
                              weatherIcon.source = resulticon;
                        } else if (rainVolume >= snowVolume/5 && snowVolume >= rainVolume/5){
                            var combined = rainVolume + snowVolume
                            if (combined < 1) {
                                resulticon = "../graphics/weather-sleet1-symbolic.svg";
                            } else if (combined >= 1 && combined < 15) {
                                resulticon = "../graphics/weather-sleet2-symbolic.svg";
                            } else if (combined >= 15) {
                                resulticon = "../graphics/weather-sleet3-symbolic.svg";
                            }
                            weatherIcon.source = resulticon;
                        } else {
                            resulticon = ""; //TODO: add mixed downfall icons, temporary use sleet icon for all
                        }
                    }
                    return resulticon;
                }
                Component.onCompleted: getRainSnowIcon(hourData["metric"].rain, hourData["metric"].snow, hourData.icon)
            }

            Label {
                id: tempLabel
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: units.gu(3)
                font.weight: Font.Light
                text: getTemp(hourData["metric"].temp, true)
            }

            Item {
                id: chanceOfPrecipRow
                width: childrenRect.width
                height: units.gu(3)
                anchors.horizontalCenter: parent.horizontalCenter
                visible: popLabel.text !== ""
                Icon {
                    id: popIcon
                    anchors.verticalCenter: parent.verticalCenter
                    color: theme.palette.normal.baseText
                    width: units.gu(1.5)
                    height: width
                    source: "../graphics/extended-information_chance-of-rain.svg"
                }
                Label {
                    id: popLabel
                    anchors.left: popIcon.right
                    anchors.leftMargin: units.gu(0.5)
                    anchors.verticalCenter: popIcon.verticalCenter
                    font.pixelSize: units.gu(2)
                    font.weight: Font.Light
                    text: hourData.pop !== undefined ? i18n.ctr("%1 will be replaces with the value, unit % for chance of precipitation, hourly forecast", "%1%").arg(hourData.pop*100) : ""
                }
            }

            Label {
                id: snowrainLabel
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.rightMargin: units.gu(2)
                font.pixelSize: units.gu(2)
                font.weight: Font.Light
                //shows the combined value of total downfall rain + snow
                text: getRainSnowValue(hourData["metric"].rain + hourData["metric"].snow, false)
            }
        }

        Rectangle {
            anchors.verticalCenter: parent.verticalCenter
            color: UbuntuColors.darkGrey
            height: hourColumn.height
            opacity: 0.2
            visible: index > 0
            width: units.gu(0.1)
        }
    }
}
