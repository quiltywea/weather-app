/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../components"
import "settings"
Page {
    id: settingsPage

    header: PageHeader {
        id: header
        title: i18n.tr("Settings")

        trailingActionBar {
            actions: [
                Action {
                    iconName: "info"
                    objectName: "aboutButton"
                    onTriggered: mainPageStack.push(Qt.resolvedUrl("../components/AboutGeneral.qml"))
                },
                Action {
                    iconName: "help"
                    objectName: "helpButton"
                    onTriggered: mainPageStack.push(Qt.resolvedUrl("../components/About.qml"))
                }
            ]
            objectName: "settingsTrailingActionBar"
        }
    }

    property bool bug1341671workaround: true

    Flickable {
        id: settingsFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded
        boundsBehavior: Flickable.StopAtBounds

        anchors {
            topMargin: settingsPage.header.height + units.gu(2)
            fill: parent
        }

        contentHeight: settingsColumn.childrenRect.height

        Column {
            id: settingsColumn
            spacing: units.gu(1.5)
            property real elementHeight: units.gu(6)

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }

            ListItem {
                width: parent.width
                height: dpp.height > 0 ? dpp.height : elementHeight
                divider.visible: false

                DataProviderPage {
                    id: dpp
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: ripg.height > 0 ? ripg.height : elementHeight
                divider.visible: false

                RefreshIntervalPage {
                    id: ripg
                    width: parent.width
                }
            }
            Label {
                text: "(" + i18n.tr("Data is only updated while the app is open and focused") + ")"
                color: theme.palette.normal.backgroundTertiaryText
                width: parent.width
                wrapMode: Text.WordWrap
                font.italic: true
                fontSize: "small"
            }

            ListItem {
                width: parent.width
                height: wupg.height > 0 ? wupg.height : elementHeight
                divider.visible: false

                WindUnitsPage {
                    id: wupg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: upg.height > 0 ? upg.height : elementHeight
                divider.visible: false

                TempUnitsPage {
                    id: upg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: rpg.height > 0 ? rpg.height : elementHeight
                divider.visible: false

                RainUnitsPage {
                    id: rpg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: tcp.height > 0 ? tcp.height : elementHeight
                divider.visible: false

                TemperatureColorPage {
                    id: tcp
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: tpg.height > 0 ? tpg.height : elementHeight
                divider.visible: false

                ThemePage {
                    id: tpg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: tbcp.height > 0 ? tbcp.height : elementHeight
                divider.visible: false

                TodayColorPage {
                    id: tbcp
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: lpg.height > 0 ? lpg.height : elementHeight
                divider.visible: false

                LocationPage {
                    id: lpg
                    width: parent.width
                }
            }
        }
    }
}
