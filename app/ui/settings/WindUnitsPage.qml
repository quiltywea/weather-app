/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../../components"

Item {
    id: unitsPage
    height: windSetting.height

    ListModel {
        id: windSpeedModel
        function initialize() {
            if (windSpeedModel.count == 0)
            // TRANSLATORS: meter per second, metric SI unit for wind speed, only use abbreviated
            windSpeedModel.append({"text": i18n.tr("m/s"), "value": "m/s"})

            // TRANSLATORS: kilometer per hour, metric unit for wind speed, only use abbreviated
            windSpeedModel.append({"text": i18n.tr("km/h"), "value": "km/h"})

            // TRANSLATORS: miles per hour, imperial unit for wind speed, only use abbreviated
            windSpeedModel.append({"text": i18n.tr("mph"), "value": "mph"})
        }
    }

    OptionSelector {
        id: windSetting
        text: i18n.tr("Wind speed unit")
        model: windSpeedModel
        containerHeight: itemHeight * model.count
        delegate: OptionSelectorDelegate {
            text: model.text
            height: units.gu(4)
        }
        onDelegateClicked: {
            settings.windUnits = model.get(index).value
            windSetting.selectedIndex = index
            refreshData(true);
        }
        Component.onCompleted: {
            /*
            The Component.onCompleted of the OptionSelector finishes BEFORE
            the onCompleted events of the delegates or the model.
            That is why the initialize() needs to be called here rather than
            in the Component.onCompleted of the ListModel.
            */
            windSpeedModel.initialize()
            for (var i = 0; i < windSpeedModel.count; ++i) {
                if (windSpeedModel.get(i).value === settings.windUnits) {
                    windSetting.selectedIndex = i
                    return
                }
            }
            windSetting.selectedIndex = 0  // in case no match is found due to broken settings
        }
    }
}
