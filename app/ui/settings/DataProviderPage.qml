/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../../components"
import "../../data/keys.js" as Keys

Item {
    id: dataProviderPage
    height: dataProviderSetting.height

    ListModel {
        id: dataProviderModel
        function initialize() {
            // TRANSLATORS: name of the weather data service provider, only translate if needed
            dataProviderModel.append({"text": i18n.tr("OpenWeatherMap"), "value": "openweathermap"})

        }
    }

    OptionSelector {
        id: dataProviderSetting
        text: i18n.tr("Data Provider")
        model: dataProviderModel
        containerHeight: itemHeight * dataProviderModel.count
        delegate: OptionSelectorDelegate {
            text: model.text
            height: units.gu(4)
        }
        onDelegateClicked: {
            settings.service = model.get(index).value
            refreshData(true);
        }
    }
    Component.onCompleted: {
        /*
        The Component.onCompleted of the OptionSelector finishes BEFORE
        the onCompleted events of the delegates or the model.
        That is why the initialize() needs to be called here rather than
        in the Component.onCompleted of the ListModel.
        */
        dataProviderModel.initialize()
        for (var i = 0; i < dataProviderModel.count; ++i) {
            if (dataProviderModel.get(i).value === settings.service) {
                dataProviderSetting.selectedIndex = i
                return
            }
        }
        dataProviderSetting.selectedIndex = 0  // in case no match is found due to broken settings
    }

    // Component.onCompleted: {
    //     // If the key file for TWC is not blank, add the service to the model
    //     if (!!Keys.twcKey) {
    //         dataProviderModel.append({ text: "The Weather Channel" })
    //     }
    // }
}
