/*
 * Copyright (C) 2019-2020 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../../components"

Item {
    id: themePage
    height: themeSetting.height

    ListModel {
        id: themeModel
        function initialize() {
            themeModel.append({"text": i18n.tr("System theme"), "value": "System"})
            themeModel.append({"text": i18n.tr("SuruDark theme"), "value": "SuruDark"})
            themeModel.append({"text": i18n.tr("Ambiance theme"), "value": "Ambiance"})
        }
    }

    OptionSelector {
        id: themeSetting
        text: i18n.tr("Style")
        model: themeModel
        containerHeight: itemHeight * themeModel.count
        delegate: OptionSelectorDelegate {
            text: model.text
            height: units.gu(4)
        }
        onDelegateClicked: {
            settings.selectedTheme = model.get(index).value
            weatherApp.setCurrentTheme();
        }
    }
    Component.onCompleted: {
        /*
        The Component.onCompleted of the OptionSelector finishes BEFORE
        the onCompleted events of the delegates or the model.
        That is why the initialize() needs to be called here rather than
        in the Component.onCompleted of the ListModel.
        */
        themeModel.initialize()
        for (var i = 0; i < themeModel.count; ++i) {
            if (themeModel.get(i).value === settings.selectedTheme) {
                themeSetting.selectedIndex = i
                return
            }
        }
        themeSetting.selectedIndex = 0  // in case no match is found due to broken settings
    }
}
