/*
 * Copyright (C) 2019-2020 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../../components"

Item {
    id: colorThemePage
    height: colorThemeSetting.height

    ListModel {
        id: colorThemeModel
        function initialize() {
            colorThemeModel.append({"colors": "no colors", "text": i18n.tr("no colors")})
            colorThemeModel.append({"colors": "blue/yellow", "text": i18n.tr("blue/yellow")})
            colorThemeModel.append({"colors": "blue/orange", "text": i18n.tr("blue/orange")})
            colorThemeModel.append({"colors": "blue/red", "text": i18n.tr("blue/red")})
        }
    }

    OptionSelector {
        id: colorThemeSetting
        text: i18n.tr("Temperature colors")
        model: colorThemeModel
        containerHeight: itemHeight * colorThemeModel.count
        delegate: OptionSelectorDelegate {
            text: i18n.tr(model.text)
            height: units.gu(4)
        }
        onDelegateClicked: setTemperatureColors(model.get(index).colors)
        Component.onCompleted: {
            /*
            The Component.onCompleted of the OptionSelector finishes BEFORE
            the onCompleted events of the delegates or the model.
            That is why the initialize() needs to be called here rather than
            in the Component.onCompleted of the ListModel.
            */
            colorThemeModel.initialize()
            for (var i = 0; i < colorThemeModel.count; ++i) {
                if (colorThemeModel.get(i).colors === settings.colorTheme) {
                    colorThemeSetting.selectedIndex = i
                    return
                }
            }
            colorThemeSetting.selectedIndex = 0  // in case no match is found due to broken settings
        }
    }
}
