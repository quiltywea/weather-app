/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../../components"

Item {
    id: refreshIntervalPage
    height: refreshIntervalSetting.height

    ListModel {
        id: refreshModel
        function initialize() {
            refreshModel.append({"interval": 600, "text": i18n.tr("%1 minute", "%1 minutes", 10).arg(10)})
            refreshModel.append({"interval": 900, "text": i18n.tr("%1 minute", "%1 minutes", 15).arg(15)})
            refreshModel.append({"interval": 1800, "text": i18n.tr("%1 minute", "%1 minutes", 30).arg(30)})
            refreshModel.append({"interval": 3600, "text": i18n.tr("%1 hour", "%1 hours", 1).arg(1)})
            refreshModel.append({"interval": 7200, "text": i18n.tr("%1 hour", "%1 hours", 2).arg(2)})
            refreshModel.append({"interval": 14400, "text": i18n.tr("%1 hour", "%1 hours", 4).arg(4)})
        }
    }

    OptionSelector {
        id: refreshIntervalSetting
        text: i18n.tr("Refresh Interval")
        model: refreshModel
        containerHeight: itemHeight * refreshModel.count
        delegate: OptionSelectorDelegate {
            text: model.text
            height: units.gu(4)
        }
        onDelegateClicked: {
            settings.refreshInterval = model.get(index).interval
            refreshData(false, true);
        }
        Component.onCompleted: {
            /*
            The Component.onCompleted of the OptionSelector finishes BEFORE
            the onCompleted events of the delegates or the model.
            That is why the initialize() needs to be called here rather than
            in the Component.onCompleted of the ListModel.
            */
            refreshModel.initialize()
            for (var i = 0; i < refreshModel.count; ++i) {
                if (refreshModel.get(i).interval === settings.refreshInterval) {
                    refreshIntervalSetting.selectedIndex = i
                    return
                }
            }
            refreshIntervalSetting.selectedIndex = 0  // in case no match is found due to broken settings
        }
    }
}
