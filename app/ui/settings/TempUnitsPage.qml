/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import "../../components"

Item {
    id: unitsPage
    height: temperatureSetting.height

    ListModel {
        id: temperatureModel
        function initialize() {
            // TRANSLATORS: degree celsius, metric unit for temperature, only use abbreviated
            temperatureModel.append({"text": i18n.tr("°C"), "value": "°C"})

            // TRANSLATORS: degree fahrenheit, imperial unit for temperature, only use abbreviated
            temperatureModel.append({"text": i18n.tr("°F"), "value": "°F"})
        }
    }

    OptionSelector {
        id: temperatureSetting
        text: i18n.tr("Temperature unit")
        model: temperatureModel
        containerHeight: itemHeight * temperatureModel.count
        delegate: OptionSelectorDelegate {
            text: model.text
            height: units.gu(4)
        }
        onDelegateClicked: {
            settings.tempScale = model.get(index).value
            refreshData(true);
        }
    }
    Component.onCompleted: {
        /*
        The Component.onCompleted of the OptionSelector finishes BEFORE
        the onCompleted events of the delegates or the model.
        That is why the initialize() needs to be called here rather than
        in the Component.onCompleted of the ListModel.
        */
        temperatureModel.initialize()
        for (var i = 0; i < temperatureModel.count; ++i) {
            if (temperatureModel.get(i).value === settings.tempScale) {
                temperatureSetting.selectedIndex = i
                return
            }
        }
        temperatureSetting.selectedIndex = 0  // in case no match is found due to broken settings
    }
}
